let currentInput = ""
let hideButtons = false;
//Will use fetch to get the greeting message using the input given by the user.
function greetingsClick() {
    fetch("http://localhost:8080/greeting/" + currentInput)
        .then(r => r.text())
        .then(text => toastResponse(text))
        .catch(err => {
            toastResponse("Error upon fetching")
        })
}
//Will use fetch to get the reversed message using the input given by the user.
function reverseClick() {
    fetch("http://localhost:8080/reverse/" + currentInput)
        .then(r => r.text())
        .then(text => toastResponse(text))
        .catch(err => {
            toastResponse("Error upon fetching")
        })
}

/**
 * Used to show the toast and set it to disapear withtin 2000ms.
 * It will disable buttons during the time the toast is shown.
 * If there's a message in the input after 2000ms the buttons will be shown again.
 * @param message - Message which should be shown within the toast.
 */
function toastResponse(message) {
    $('.toast').toast({delay: 2000})
    $('.toast').toast('show')
    hideButtons = true;
    document.getElementById("buttons").style.display = "none";
    document.getElementById("toastT").innerHTML = message;
    setTimeout(() => {
        hideButtons = false
        if(currentInput !== "") {
            document.getElementById("buttons").style.display = "block";
        }
    }, 2000)
}

/**
 * Handles input from the user.
 * When the input isn't empty buttons will be shown.
 * When the input is empty buttons are hidden.
 * @param event - input event.
 */
function handleInput(event) {
    currentInput = event.value;
    if(currentInput !== "" && !hideButtons) {
        document.getElementById("buttons").style.display = "block";
    }
    else {
        document.getElementById("buttons").style.display = "none";
    }
}
