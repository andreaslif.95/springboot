package se.experis.com.SpringBoot.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class StringController {
    /**
     * A getMethod which creates a string formatting the input_string by contcatinating it with a greeting message.
     * @param input_String - String which should be returned within the greeting.
     * @return - (string) Greetings + input_string + !
     */
    @RequestMapping(value = "/greeting/{input_String}", method = RequestMethod.GET)
    public String getGreeting(@PathVariable String input_String) {
        return "Greetings " + input_String + " !";
    }
    /**
     * A getMethod which reverses the string which comes as the PathVariable
     * @param input_String - String which should be returned reversed
     * @return - (String) reversed input_string
     */
    @RequestMapping(value = "/reverse/{input_String}", method = RequestMethod.GET)
    public String getStringToReverse(@PathVariable String input_String) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = input_String.length()-1; i >= 0; i--) {
            stringBuilder.append(input_String.charAt(i));
        }
        return stringBuilder.toString();
    }
}
