Task: Spring Boot
Create a Spring web application.

- [x] Create an Index.html and put it inside the resources/static folder and it will not need a mapping, it will be the default for your server.

This page must have the following:

- [x] An input field (text box)

- [x] A Greeting button:
- [x] Returns a greeting string to the user, if their put something in the input field it must greet them.

- [x] A reverse button:
- [x] Returns a reverse of the string that they entered.

- [x] The page must use Bootstrap for styling (use a CDN).

